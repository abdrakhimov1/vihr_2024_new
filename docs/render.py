# -*- coding: utf-8 -*-
from jinja2 import Template
from collections import defaultdict
from utils import mark_grath, comul_command_grath, order_grath, comul_grath, order_command_grath, mark_command_grath
import json
import requests

# User
# Calc statistics

all_data_url = r' https://sheets.googleapis.com/v4/spreadsheets/1pj_dMklTYnjIuKdN_YtXc8sA8JYnLY22FVKNG7L_TJc/values/Лист2?dateTimeRenderOption=FORMATTED_STRING&majorDimension=ROWS&valueRenderOption=FORMATTED_VALUE&key=AIzaSyCCZbPf-TGUWkqGkVPy48j4hiQYSaTFauw'


all_data_path = 'out_data/all_data.json'
req_all_data = requests.get(all_data_url)
all_data = req_all_data.json()

with open(all_data_path, 'w') as f:
    json.dump(all_data, f)

with open(all_data_path) as f:
    all_data = json.load(f)

cols_all_data = all_data.get("values")[0]
content_all_data = all_data.get("values")[1:]

index_col_map = { item: cols_all_data.index(item) for item in cols_all_data }

player_statistic = defaultdict(dict)

radar_data_all = {}
radar_labels = [ "Mark", "Assist", "Score", "Falls", "Goal"]
order_label = ["Goal", "Assist", "Score", "Falls"]

command_data = {} # 

# aggregate data
for i in content_all_data:
    current_player = i[index_col_map["Player"]]

    # Teams
    stat_value = "grath_team"
    current_value = player_statistic[current_player].get(stat_value, [])
    current_value_x = str(i[index_col_map["Team"]])
    current_value.append(current_value_x)
    player_statistic[current_player][stat_value] = current_value

    for stat_value in order_label:
        curent_value = int(i[index_col_map[stat_value]])
        curent_value = curent_value if curent_value > -1 else 0
        player_statistic[current_player][stat_value] = player_statistic[current_player].get(stat_value, 0) + curent_value 

        # collect data
        stat_data_value = f"data_{stat_value}"
        current_stat_data_value = player_statistic[current_player].get(stat_data_value, [])
        current_stat_data_value.append(curent_value)
        player_statistic[current_player][stat_data_value] = current_stat_data_value



    stat_value = "Mark"
    curent_value = int(i[index_col_map[stat_value]])
    if curent_value > -1:
        stat_data_value = f"data_{stat_value}"
        stat_data_value_x = f"data_{stat_value}_x"
        current_stat_data_value = player_statistic[current_player].get(stat_data_value, [])
        current_stat_data_value.append(curent_value)
        player_statistic[current_player][stat_data_value] = current_stat_data_value


        current_stat_data_value_x = player_statistic[current_player].get(stat_data_value_x, [])
        current_stat_data_value_x.append(current_value_x)
        player_statistic[current_player][stat_data_value_x] = current_stat_data_value_x

    stat_value = "Matches"
    curent_value = int(i[index_col_map["Mark"]])
    curent_value = 1 if curent_value != -1 else 0
    player_statistic[current_player][stat_value] = player_statistic[current_player].get(stat_value, 0) + curent_value 

    stat_value = "Missed"
    curent_value = int(i[index_col_map["Mark"]])
    curent_value = 1 if curent_value == -1 else 0
    player_statistic[current_player][stat_value] = player_statistic[current_player].get(stat_value, 0) + curent_value 

    
    # radar data
    for l in radar_labels:
        current_value = int(i[index_col_map[l]])
        if l == 'Mark'and current_value < 0:
            continue
        current_value = current_value if current_value > -1 else 0
        # Todo Mark исключить < 0
        tmp_radar_data = radar_data_all.get(l, [])
        tmp_radar_data.append(current_value)
        radar_data_all[l] = tmp_radar_data

         # common metrics
        team_data = command_data.get(current_value_x, {})
        metric_data = team_data.get(f"data_{l}", [])
        metric_data.append(current_value)
        team_data[f"data_{l}"] = metric_data
        command_data[current_value_x] = team_data

command_data["grath_team"] = list(command_data.keys())

table_command_data = {}
for c in command_data["grath_team"] :
    table_command_data[c] = {"Team": c}
    for l in radar_labels:
        if  l == 'Mark':
            table_command_data[c][l] = "{:.2f}".format(sum(command_data[c][f"data_{l}"]) / len(command_data[c][f"data_{l}"]))
        else:
            table_command_data[c][l] = sum(command_data[c][f"data_{l}"])
            

# add graths
mark_command_grath(command_data)
for l in order_label:
    order_command_grath(command_data, l)
    comul_command_grath(command_data, l)

# add graths
for current_player in player_statistic.keys():
    mark_grath(current_player, player_statistic)
    for l in order_label:
        order_grath(current_player, player_statistic, l)
        comul_grath(current_player, player_statistic, l)




# Главная
main_template = Template(open('templates/main.template').read())
data_path = 'out_data/main.json'

# load data
with open(data_path) as f:
    data = json.load(f)


table_data_path = 'out_data/table_data.json'
with open(table_data_path) as f:
    table_data = json.load(f)

with open("docs/common.md", "w") as f:
    f.write(main_template.render(
        items=command_data['graths'].values(),
        table_data=table_command_data.values())
        )


# calc radar value
for l in radar_labels:
    radar_data_all[f"avg_{l}"] = sum(radar_data_all[l]) / len(radar_data_all[l])

radar_data_all["avg_Mark"] = radar_data_all["avg_Mark"] / 10



users_data_url = r'https://sheets.googleapis.com/v4/spreadsheets/1pj_dMklTYnjIuKdN_YtXc8sA8JYnLY22FVKNG7L_TJc/values/%D0%9B%D0%B8%D1%81%D1%823?dateTimeRenderOption=FORMATTED_STRING&majorDimension=ROWS&valueRenderOption=FORMATTED_VALUE&key=AIzaSyCCZbPf-TGUWkqGkVPy48j4hiQYSaTFauw'

user_data_path = 'out_data/user_data.json'
req_user_data = requests.get(users_data_url)
user_data = req_user_data.json()
with open(user_data_path, 'w') as f:
    json.dump(req_user_data.json(), f)

with open(user_data_path) as f:
    user_data = json.load(f)

cols = user_data.get("values")[0]
user_content_data = user_data.get("values")[1:]

user_tmpl = open('templates/player.template').read()

all_users_data = []
for number, user in enumerate(user_content_data, 1):
    user_template = Template(user_tmpl)
    tmp_user_data = {
        "Number": number
    }
    for i, col_name in enumerate(cols):
        tmp_user_data[col_name] = user[i]
    tmp_user_data.update(player_statistic[user[cols.index('Player')]])
    # Поюзерно теперь считается правильно
    tmp_user_data['Mark'] =  "{:.2f}".format(sum(tmp_user_data["data_Mark"]) / len(tmp_user_data["data_Mark"]))
    # calc radar value
    radar_data = []
    radar_team_data = []
    for l in radar_labels:
        radar_team_data.append(radar_data_all[f"avg_{l}"])
        if l != "Mark":
            matches = tmp_user_data["Matches"] if tmp_user_data["Matches"] > 0 else 1
            radar_data.append(tmp_user_data[l] / matches)
        else: 
            radar_data.append(float(tmp_user_data[l]) / 10) 



    with open(f"docs/users/player_{tmp_user_data.get('Number', 0)}.md", "w") as f:
        f.write(user_template.render(radar_data=radar_data,
                                    radar_team_data=radar_team_data,
                                    radar_labels=json.dumps(radar_labels),
                                    items=tmp_user_data["graths"].values(),
                                    **tmp_user_data)
                                    )

    all_users_data.append(tmp_user_data)


main_player_template = Template(open('templates/player_main.template').read())
with open("docs/users/main.md", "w") as f:
    f.write(main_player_template.render(players=all_users_data))



# Config

config_tmpl = open('templates/config.template').read()

with open(f"docs/.vuepress/config.js", "w") as f:
    f.write(Template(config_tmpl).render(all_players=all_users_data))




