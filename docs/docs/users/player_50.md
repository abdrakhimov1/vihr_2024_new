# Игрок: Улюмджинов Темир

## Общие характеристики

- Номер  50
- Провел матчей за сезон 9
- Пропустил матчей за сезон 7
- Голы  0
- Асисты 1
- Очки 1
- Средняя оценка 6.777777777777778

## Отзыв тренера 

Классно провел первый сезон в нашей команде. Я очень рад, что ты к нам присоеденился и влился в наш дружный коллектив. Твоя трудолюбивость и способность выполнять поставленные задачи делает из тебя первоклассного защитника. В некоторых моментах ты сильно перестраховываешься и не даешь волю креативу. Будем развивать нестандартные подходы в твоей игре, чтобы приносить еще больше пользы команде. Продолжай в том же духе и не забывай иногда отдыхать от хоккея)

## Отзыв  GPT 

Темир провёл сезон на хорошем уровне, показав надёжную игру в обороне. Он продемонстрировал умение начинать атаку правильно, делая первый пас так, чтобы его точно принимали партнёры. Это позволяет избегать ситуаций, когда неточные передачи приводят к необходимости постоянно обороняться.
Однако Темиру следует обратить внимание на свою игру в атаке. Хотя он и показал несколько хороших моментов, когда бросал по воротам, его потенциал позволяет делать более интересные вещи. На тренировках необходимо разобрать, как Темир может помогать партнёрам в атаке и начать забивать голы.

## Рекомендации тренера 

test

## Инфографика


::: chart A Radar Chart

```json
{
  "type": "radar",
  "data": {
    "labels": ["Mark", "Assist", "Score", "Falls", "Goal"],
    "datasets": [
      {
        "label": "VKHR",
        "data": [0.5926174496644295, 0.125, 0.29910714285714285, 0.45089285714285715, 0.19196428571428573],
        "fill": true,
        "backgroundColor": "rgba(255, 99, 132, 0.2)",
        "borderColor": "rgb(255, 99, 132)",
        "pointBackgroundColor": "rgb(255, 99, 132)",
        "pointBorderColor": "#fff",
        "pointHoverBackgroundColor": "#fff",
        "pointHoverBorderColor": "rgb(255, 99, 132)"
      },
      {
        "label": "Улюмджинов Темир",
        "data": [0.6777777777777778, 0.1111111111111111, 0.1111111111111111, 1.0, 0.0],
        "fill": true,
        "backgroundColor": "rgba(54, 162, 235, 0.2)",
        "borderColor": "rgb(54, 162, 235)",
        "pointBackgroundColor": "rgb(54, 162, 235)",
        "pointBorderColor": "#fff",
        "pointHoverBackgroundColor": "#fff",
        "pointHoverBorderColor": "rgb(54, 162, 235)"
      }
    ]
  },
  "options": {
    "elements": {
      "line": {
        "borderWidth": 3
      }
    }
  }
}
```

:::

 

::: echarts Mark

```js


var x_in_data = ['Снежные барсы', 'Белые медведи', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [5, 4, 8, 8, 7, 6, 8, 8, 7];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Goal

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Comulative Goal

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "line",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Assist

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Comulative Assist

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "line",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Score

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Comulative Score

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "line",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Falls

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 2, 2, 0, 0, 0, 0, 2, 0, 1, 2, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Comulative Falls

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 2, 4, 4, 4, 4, 4, 6, 6, 7, 9, 9];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team: " + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "line",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::




