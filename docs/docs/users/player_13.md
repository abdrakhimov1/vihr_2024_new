# Игрок: Хатырев Василий

## Общие характеристики

- Номер  13
- Провел матчей за сезон 14
- Пропустил матчей за сезон 2
- Голы  6
- Асисты 6
- Очки 12
- Средняя оценка 5.125

## Отзыв тренера 

test

## Отзыв  GPT 

Василий провёл сезон хорошо, демонстрируя уверенную игру в атаке. Он закрывал зону, отдавал передачи, бросал и обыгрывал соперников. Однако ему необходимо работать над игрой в зоне обороны и на пятаке.
Василий грамотно играл на пятаке, хорошо пользуясь своими габаритами. Он показал активную поддержку пятёрки в атаке, хотя и пропустил несколько тренировок.
Однако Василию следует улучшить свою игру в защите. Он плохо возвращается в защиту и теряет позицию при позиционной защите. Также Василий дважды отдал ненужную передачу, что привело к голу.
Василию следует уделять больше внимания тренировкам и работе над ошибками, чтобы улучшить свою игру в защите и не допускать ненужных передач.

## Рекомендации тренера 

test

## Инфографика


 

::: echarts Goal

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [1, 2, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Assist

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 2, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Score

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [2, 2, 1, 0, 0, 1, 1, 1, 1, 1, 0, 2, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Falls

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [2, 2, 0, 2, 0, 1, 1, 4, 1, 0, 0, 0, 1, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Mark

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [7, 6, 7, 4, 6, 6, 3, 10, 6, 6, 0, 6, 5, 0, 6, 4];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::





::: chart A Radar Chart

```json
{
  "type": "radar",
  "data": {
    "labels": ["Mark", "Assist", "Score", "Falls", "Goal"],
    "datasets": [
      {
        "label": "VKHR",
        "data": [3.924107142857143, 0.125, 0.29910714285714285, 0.45089285714285715, 0.19196428571428573],
        "fill": true,
        "backgroundColor": "rgba(255, 99, 132, 0.2)",
        "borderColor": "rgb(255, 99, 132)",
        "pointBackgroundColor": "rgb(255, 99, 132)",
        "pointBorderColor": "#fff",
        "pointHoverBackgroundColor": "#fff",
        "pointHoverBorderColor": "rgb(255, 99, 132)"
      },
      {
        "label": "Хатырев Василий",
        "data": [5.125, 0.42857142857142855, 0.8571428571428571, 1.0, 0.42857142857142855],
        "fill": true,
        "backgroundColor": "rgba(54, 162, 235, 0.2)",
        "borderColor": "rgb(54, 162, 235)",
        "pointBackgroundColor": "rgb(54, 162, 235)",
        "pointBorderColor": "#fff",
        "pointHoverBackgroundColor": "#fff",
        "pointHoverBorderColor": "rgb(54, 162, 235)"
      }
    ]
  },
  "options": {
    "elements": {
      "line": {
        "borderWidth": 3
      }
    }
  }
}
```

:::