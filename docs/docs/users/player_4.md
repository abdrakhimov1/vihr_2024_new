# Игрок: Бут Дмитрий

## Общие характеристики

- Номер  4
- Провел матчей за сезон 5
- Пропустил матчей за сезон 11
- Голы  0
- Асисты 0
- Очки 0
- Средняя оценка 1.625

## Отзыв тренера 

test

## Отзыв  GPT 

Мало данных

## Рекомендации тренера 

test

## Инфографика


 

::: echarts Goal

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Assist

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Score

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Falls

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Mark

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [6, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 5, 0, 0, 5, 6];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\""
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::





::: chart A Radar Chart

```json
{
  "type": "radar",
  "data": {
    "labels": ["Mark", "Assist", "Score", "Falls", "Goal"],
    "datasets": [
      {
        "label": "VKHR",
        "data": [3.924107142857143, 0.125, 0.29910714285714285, 0.45089285714285715, 0.19196428571428573],
        "fill": true,
        "backgroundColor": "rgba(255, 99, 132, 0.2)",
        "borderColor": "rgb(255, 99, 132)",
        "pointBackgroundColor": "rgb(255, 99, 132)",
        "pointBorderColor": "#fff",
        "pointHoverBackgroundColor": "#fff",
        "pointHoverBorderColor": "rgb(255, 99, 132)"
      },
      {
        "label": "Бут Дмитрий",
        "data": [1.625, 0.0, 0.0, 0.0, 0.0],
        "fill": true,
        "backgroundColor": "rgba(54, 162, 235, 0.2)",
        "borderColor": "rgb(54, 162, 235)",
        "pointBackgroundColor": "rgb(54, 162, 235)",
        "pointBorderColor": "#fff",
        "pointHoverBackgroundColor": "#fff",
        "pointHoverBorderColor": "rgb(54, 162, 235)"
      }
    ]
  },
  "options": {
    "elements": {
      "line": {
        "borderWidth": 3
      }
    }
  }
}
```

:::