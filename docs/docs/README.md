---
home: true
heroImage: ./vkhr.png
tagline: Статистика и итоги сезона 2023/2024 дхк Вихрь
actions:
  - text: Общая статистика
    link: /common.md
    type: secondary
  - text: Статистика по игрокам
    link: /users/main.md
    type: secondary
  - text: Вратари
    link: /goalkeeper.md
    type: secondary
 
features:
  - title: Статистика по команде
    details: Можно посмотреть статистику по команде и общие показатели в течение года.
  - title: Статистика по игрокам
    details: Сводная статистика по каждому игроку, оценки, комментарии тренера, комментарии искусственного интеллекта, рекомендации по дальнейшей работе.
  - title: Вратарская статичтика
    details: Сводная статистика по вратарям

footer:  ДХК Вихрь
---
