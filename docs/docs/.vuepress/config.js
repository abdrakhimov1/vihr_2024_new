import { viteBundler } from '@vuepress/bundler-vite'
import { defaultTheme } from '@vuepress/theme-default'
import { defineUserConfig } from 'vuepress'
import { mdEnhancePlugin } from "vuepress-plugin-md-enhance";
import { createPage } from 'vuepress/core'


export default defineUserConfig({
  bundler: viteBundler(),
  title: "VKHR Site",
  description: "Это оoooooчень полезный сайт",
  lang: 'ru-RU',
  head: [
      ['link', { rel: 'icon', href: '/vkhr.png' }]
  ],
  theme: defaultTheme(
    {
      logo: '/vkhr.png',
      navbar: [
        {
          text: 'Главная',
          link: '/',
        },
       {
          text: 'Общая статистика',
          link: '/common.md',
        },
        {
          text: 'Игроки',
          children: [
             
            {
              text: 'Абдрахимов Алексей #71',
              link: '/users/player_71.md'
            },
             
            {
              text: 'Бородаенко Виктор #72',
              link: '/users/player_72.md'
            },
             
            {
              text: 'Бородаенко Матвей #25',
              link: '/users/player_25.md'
            },
             
            {
              text: 'Бут Дмитрий #44',
              link: '/users/player_44.md'
            },
             
            {
              text: 'Вахрулин Роман #73',
              link: '/users/player_73.md'
            },
             
            {
              text: 'Галкин Иван #69',
              link: '/users/player_69.md'
            },
             
            {
              text: 'Мельников Кирилл #5',
              link: '/users/player_5.md'
            },
             
            {
              text: 'Мурашев Павел  #17',
              link: '/users/player_17.md'
            },
             
            {
              text: 'Ситдиков Кирилл #87',
              link: '/users/player_87.md'
            },
             
            {
              text: 'Сычков Максим #19',
              link: '/users/player_19.md'
            },
             
            {
              text: 'Титух Тимофей #55',
              link: '/users/player_55.md'
            },
             
            {
              text: 'Улюмджинов Темир #50',
              link: '/users/player_50.md'
            },
             
            {
              text: 'Хатырев Василий #77',
              link: '/users/player_77.md'
            },
             
            {
              text: 'Other #0',
              link: '/users/player_0.md'
            },
            
            
          ]
      }]}
  ),
  plugins: [
    mdEnhancePlugin({
        chart: true,
        echarts: true,
    }),
  ]
})