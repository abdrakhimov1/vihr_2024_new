# Общая статистика

| Команда  | Сейвы | Пропущенные шайбы | Процент отраженных | Коэффицент надежности | Оценка |
| ----- | -----| -------|-------|-------|-------|
 | Amber Stone | 35  | 12  | 74.5  | 16  | 10  |
 | Гранит | 20  | 4  | 83.3  | 5.333  | 10  |
 | Друзья | 57  | 3  | 95 | 4  | 10 |
 | Ice battle | 25  | 13  | 65.8  | 17.333  | 10 |
 | Энергия | 17  | 4  | 81  | 5.333  | 10  |
 | Снежные барсы | 24  | 10  | 70.6  | 13.333  | 10  |
 | Белые медведи  | 37  | 12  | 75.5  | 16  | 10  |
 | Белые медведи (2) | 32	  | 13  | 71.1 | 17.33  | 10  |
 | Энергия (2) | 27  | 3  | 88  | 4  | 10  |
 | Снайпер | 28  | 8  | 77.8  | 10.667  | 10  |
 | Снайпер (2) | 27  | 9  | 75  | 12  | 10  |
 | Гранит (2) | 21  | 4  | 84  | 5.333  | 10  |
 | Металлург | 29  | 8  | 78.4  | 10.667  | 10  |
 | Реванш | 28  | 11  | 71.8  | 14.667  | 10  |
 | Лидер | 34  | 8  | 81.0  | 10.667  | 10  |
 | Лидер (2) | 26  | 8  | 76.5  | 10.667  | 10  |


# Игрок: Махмутов Динияр

## Общие характеристики

- Номер  31
- Провел матчей за сезон 16
- Пропустил матчей за сезон 0
- Сейвы  467
- Пропущенные шайбы 130
- Средний процент отраженных шайб 72.16
- Средняя оценка 10.0

## Отзыв тренера 

Очень рад, что ты являешься частью нашей команды. Надежный и спокойный вратарь, который может подтащить в трудную минуту. Твоя уверенная игра иногда расслабляла наших полевых игроков, потому что они всегда уверены в том, что ты подтащишь все их косяки. Во многом из-за этого у тебя было так много работы в этом сезоне, с которой ты, несомненно, справился. Для меня как для полевого игрока вратарь моей команды всегда лучший, поэтому за все игры у тебя стоят оценки 10. Возможно, это необъективно, но я считаю, что во всех наших матчах ты был лучшим вратарем на льду. Спасибо за отличный сезон. :)

## Рекомендации тренера 

Запросить у тренера вратарей

## Инфографика


::: chart Общие показатели

```json
{
  "type": "radar",
  "data": {
    "labels": ["Сейвы", "Пропущенные шайбы", "Процент отраженных", "Коэффицент надежности (*10)", "Оценка (*10)"],
    "datasets": [
      {
        "label": "Махмутов Динияр",
        "data": [467, 130, 72, 110, 100],
        "fill": true,
        "backgroundColor": "rgba(54, 162, 235, 0.2)",
        "borderColor": "rgb(54, 162, 235)",
        "pointBackgroundColor": "rgb(54, 162, 235)",
        "pointBorderColor": "#fff",
        "pointHoverBackgroundColor": "#fff",
        "pointHoverBorderColor": "rgb(54, 162, 235)"
      }
    ]
  },
  "options": {
    "elements": {
      "line": {
        "borderWidth": 3
      }
    }
  }
}
```

:::

::: echarts Оценка

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Сейвы

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [35, 20, 57, 25, 17, 24, 37, 32, 27, 28, 27, 21, 29, 28, 34, 26];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Суммарно по сезону Сейвы

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [35, 55, 112, 137, 154, 178, 215, 247, 274, 302, 329, 350, 379, 407, 441, 467];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "line",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Пропущенные шайбы

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [12, 4, 3, 13, 4, 10, 12, 13, 3, 8, 9, 4, 8, 11, 8, 8];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


 

::: echarts Суммарно по сезону пропущенные шайбы

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [12, 16, 19, 32, 36, 46, 58, 71, 74, 82, 91, 95, 103, 114, 122, 130];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "line",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::
 
::: echarts Процент отраженных шайб

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [74.5, 83.3, 95, 65.8, 81, 70.6, 75.5, 71.1, 88, 77.8, 75, 84, 78.4, 71.8, 81.0, 76.5];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::


::: echarts Коэффицент надежности

```js


var x_in_data = ['Amber Stone', 'Гранит', 'Друзья', 'Ice battle', 'Энергия', 'Снежные барсы', 'Белые медведи', 'Белые медведи (2)', 'Энергия (2)', 'Снайпер', 'Снайпер (2)', 'Гранит (2)', 'Металлург', 'Реванш', 'Лидер', 'Лидер (2)'];
var y_in_data = [16, 5.333, 4, 17.333, 5.333, 13.333, 16, 17.33, 4, 10.667, 12, 5.333, 10.667, 14.667, 10.667, 10.667];

  
const option = {
  tooltip: {
    trigger: "axis",
    formatter: function (params) {
      params = params[0];
      return (
        "Team:" + "\"" + params.axisValue + "\"" + " - " + params.value
      );
    },
    axisPointer: {
      animation: false,
    },
  },
  xAxis: {
    type: "category",
    data: x_in_data
  },
  yAxis: {
    type: "value",
    data: y_in_data
  },
  toolbox: {
    show: true,
    feature: {
      mark: {
        show: true,
      },
      dataView: {
        show: true,
        readOnly: false,
      },
      restore: {
        show: true,
      },
      saveAsImage: {
        show: true,
      },
    },
  },
  series: [
    {
      type: "bar",
      data: y_in_data,
      labels: x_in_data
    },
  ],
};
```

:::
