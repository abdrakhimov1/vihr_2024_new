def mark_grath(current_player, player_statistic):
    stat_value = 'Mark'
    stat_value_grath = f"grath_{stat_value}"
    graths = player_statistic[current_player].get("graths", {})
    grath = graths.get(stat_value_grath, {})
    grath["title"] = "Оценка"
    grath["type"] = "bar"
    grath["y_data"] = player_statistic[current_player][f"data_{stat_value}"]
    grath["x_data"] = player_statistic[current_player][f"data_{stat_value}_x"]
    graths[stat_value_grath] = grath
    player_statistic[current_player]["graths"] = graths



def order_grath(current_player, player_statistic, stat_value):
    d = {
        "Goal": "Голы",
        "Assist": "Передачи",
        "Score": "Очки",
        "Falls": "Фолы"
         }
    stat_value_grath = f"grath_{stat_value}"
    graths = player_statistic[current_player].get("graths", {})
    grath = graths.get(stat_value_grath, {})
    grath["title"] = d.get(stat_value, "unknown")
    grath["type"] = "bar"
    grath["y_data"] = player_statistic[current_player][f"data_{stat_value}"]
    grath["x_data"] = player_statistic[current_player]["grath_team"]
    graths[stat_value_grath] = grath
    player_statistic[current_player]["graths"] = graths


def order_command_grath(data, stat_value):
    d = {
        "Goal": "Голы",
        "Assist": "Передачи",
        "Score": "Очки",
        "Falls": "Фолы"
         }
    stat_value_grath = f"grath_{stat_value}"
    graths = data.get("graths", {})
    grath = graths.get(stat_value_grath, {})
    grath["title"] = d.get(stat_value, "unknown")
    grath["type"] = "bar"
    y_data = []
    for c in data["grath_team"]:
        k = f"data_{stat_value}"
        y_data.append(sum(data[c][k]))
    grath["y_data"] = y_data
    grath["x_data"] = data["grath_team"]
    graths[stat_value_grath] = grath
    data["graths"] = graths

def mark_command_grath(data):
    stat_value_grath = "grath_Mark"
    graths = data.get("graths", {})
    grath = graths.get(stat_value_grath, {})
    grath["title"] = "Оценка"
    grath["type"] = "bar"
    y_data = []
    for c in data["grath_team"]:
        k = "data_Mark"
        y_data.append(sum(data[c][k]) / len(data[c][k]))
    grath["y_data"] = y_data
    grath["x_data"] = data["grath_team"]
    graths[stat_value_grath] = grath
    data["graths"] = graths


def comul_command_grath(data, stat_value):
    d = {
        "Goal": "Голы",
        "Assist": "Передачи",
        "Score": "Очки",
        "Falls": "Фолы"
         }
    stat_value_grath = f"comul_grath_{stat_value}"
    graths = data.get("graths", {})
    grath = graths.get(stat_value_grath, {})
    grath["title"] = f"Суммарно по сезону { d.get(stat_value, 'unknown')}"
    grath["type"] = "line"
    y_data = []
    for c in data["grath_team"]:
        k = f"data_{stat_value}"
        y_data.append(sum(data[c][k]))

    tmp = 0
    out = []
    for i in y_data:
        out.append(tmp+i)
        tmp = out[-1]

    grath["y_data"] = out
    grath["x_data"] = data["grath_team"]
    graths[stat_value_grath] = grath
    data["graths"] = graths


def comul_grath(current_player, player_statistic, stat_value):
    d = {
        "Goal": "Голы",
        "Assist": "Передачи",
        "Score": "Очки",
        "Falls": "Фолы"
         }
    stat_value_grath = f"grath_{stat_value}_comul"
    graths = player_statistic[current_player].get("graths", {})
    grath = graths.get(stat_value_grath, {})

    grath["title"] = f"Суммарно по сезону { d.get(stat_value, 'unknown')}"
    y_data = player_statistic[current_player][f"data_{stat_value}"]
    tmp = 0
    out = []
    for i in y_data:
        out.append(tmp+i)
        tmp = out[-1]
        
    grath["y_data"] = out
    grath["x_data"] = player_statistic[current_player]["grath_team"]
    grath["type"] = "line"

    graths[stat_value_grath] = grath
    player_statistic[current_player]["graths"] = graths
